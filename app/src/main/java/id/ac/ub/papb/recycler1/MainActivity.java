package id.ac.ub.papb.recycler1;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    RecyclerView rv1;
    public static String TAG = "RV1";
    EditText name;
    EditText nim;
    Button submit;

    private ArrayList<String> dataSet;
    private ArrayList<String> addDataSet;
    private MahasiswaAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        rv1 = findViewById(R.id.rv1);
        ArrayList<Mahasiswa> data = getData();
        adapter = new MahasiswaAdapter(this, data);
        rv1.setAdapter(adapter);
        rv1.setLayoutManager(new LinearLayoutManager(this));

        submit = findViewById(R.id.bt1);
        name = findViewById(R.id.etName);
        nim = findViewById(R.id.etNIM);

        submit.setOnClickListener(this);

        dataSet = new ArrayList<>();
        addDataSet = new ArrayList<>();

        dataSet();
    }

    public ArrayList getData() {
        ArrayList<Mahasiswa> data = new ArrayList<>();
        List<String> nim = Arrays.asList(getResources().getStringArray(R.array.nim));
        List<String> nama = Arrays.asList(getResources().getStringArray(R.array.nama));
        for (int i = 0; i < nim.size(); i++) {
            Mahasiswa mhs = new Mahasiswa();
            mhs.nim = nim.get(i);
            mhs.nama = nama.get(i);
            Log.d(TAG,"getData "+mhs.nim);
            data.add(mhs);
        }
        return data;
    }
    public Mahasiswa dataSet() {
        ArrayList<Mahasiswa> data = new ArrayList<>();
        String NAMA = name.getText().toString();
        String NIM = nim.getText().toString();
        Mahasiswa mhs = new Mahasiswa();
        mhs.nim = NIM;
        mhs.nama = NAMA;
        data.add(mhs);
        return mhs;
    }

    @Override
    public void onClick(View view) {
        ((MahasiswaAdapter)adapter).add(dataSet.size(), dataSet());
        name.setText("Nama");
        nim.setText("NIM");


    }
}