package id.ac.ub.papb.recycler1;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

public class Activity2 extends AppCompatActivity {

    TextView nama,nim;
    private String getName,getNIM;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_2);

        nama = findViewById(R.id.tvNama2);
        nim = findViewById(R.id.tvNim2);

        Bundle bun = getIntent().getExtras();
        getNIM = bun.getString("data1");
        getName = bun.getString("data2");

        nim.setText("NIM : " + getNIM);
        nama.setText("Nama : " + getName);
    }


}